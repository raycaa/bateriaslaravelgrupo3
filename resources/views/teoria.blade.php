@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Teorias</h1>
        <hr>
        <h2>Descrição das pilhas:</h2>
        <p>
            Pilhas são sistemas eletroquímicos que produzem energia elétrica por meio de reações de oxirredução espontâneas. São classificadas como células galvânicas ou células voltaicas. 
            A energia química se transforma em energia elétrica por meio do fluxo ordenado de elétrons gerados nas reações.
        </p>
        <img src="../imgs/panasonic.jpg" width="150px">

        <p>
            Uma pilha convencional é descartada quando sua carga acaba ou fica em nível insuficiente de energia (fraca). Com uma pilha recarregável, basta utilizar um aparelho adequado para que sua carga de energia seja restabelecida. Com isso, a pilha pode ser utilizada novamente.
        </p>
        <img src="../imgs/uatek.jpg" width="150px">
        <h3>Descrição de baterias:</h3>
        <p>
            As baterias são conjuntos de pilhas ligadas em série, ou seja, são dispositivos eletroquímicos nos quais ocorrem reações de oxidorredução, produzindo uma corrente elétrica. Podem ser chamadas ainda de pilhas secundárias, baterias secundárias ou acumuladores.
        </p>
        <img src="../imgs/golite.jpg" width="150px">
    </main>
    @endsection

@section('rodape')
    <h4>Rodapé da página teoria</h4>
@endsection
