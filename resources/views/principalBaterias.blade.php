@extends('templates.base')

@section('conteudo')

<main>
    <h1>Turma: 2D1 - Grupo 1</h1>
    <h2>Participantes</h2>
    <hr>
    <table class="table table-striped table-bordered">
    <tr>
        <td>Matricula    </td>
        <td>Nome</td>
        <td>Função</td>
    </tr>
    <tr>
        <td>0072524</td>
        <td>Rayca Lopes</td>
        <td>Gerente</td>
    </tr>
    <tr>
        <td>0072540</td>
        <td>Pedro Ferreira</td>
        <td>Desenvolvedor de Banco de Dados</td>
    </tr>
    <tr>
        <td>0072532</td>
        <td>Tiago Lopes</td>
        <td>Desenvolvedor de Banco de dados</td>
    </tr>
    <tr>
        <td>0072553</td>
        <td>Walmir de Jesus</td>
        <td>Desenvolvedor de CSS</td>
    </tr>
   </table>
</main>

@endsection

@section('rodape')
    <h4>Rodapé da página principal</h4>
@endsection