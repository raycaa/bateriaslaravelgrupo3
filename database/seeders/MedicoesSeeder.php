<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('medicoes')->insert(
            [
                [
                    'pilha_bateria' => 'Pilha Alcalina Duracel AA',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente' => 2800,
                    'tensao_sem_carga' => 1.30,
                    'tensao_com_carga' => 1.331,
                    'resistencia_carga' => 23.8,
                ],                
                [
                    'pilha_bateria' => 'Bateria Elgin (Recarregável)',
                    'tensao_nominal' => 9.0,
                    'capacidade_corrente' => 250,
                    'tensao_sem_carga' => 2.97,
                    'tensao_com_carga' => 7.35,
                    'resistencia_carga' => 23.8,
                ],                
                [
                    'pilha_bateria' => 'Bateria Golite',
                    'tensao_nominal' => 9,
                    'capacidade_corrente' => 500,
                    'tensao_sem_carga' => 3.12,
                    'tensao_com_carga' => 5.96,
                    'resistencia_carga' => 23.8,
                ],
                [
                    'pilha_bateria' => 'Pilha JYX (Recarregável)',
                    'tensao_nominal' => 4.2,
                    'capacidade_corrente' => 9800,
                    'tensao_sem_carga' => 2.77,
                    'tensao_com_carga' => 2.81,
                    'resistencia_carga' => 23.8,
                ],
                [
                    'pilha_bateria' => 'Pilha Luatek (Recarregável)',
                    'tensao_nominal' => 3.7,
                    'capacidade_corrente' => 1200,
                    'tensao_sem_carga' => 2.53,
                    'tensao_com_carga' => 2.6,
                    'resistencia_carga' => 23.8,
                ],
                [
                    'pilha_bateria' => 'Pilha Panasonic AA',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente' => 2800,
                    'tensao_sem_carga' => 1.27,
                    'tensao_com_carga' => 1.4,
                    'resistencia_carga' => 23.8,
                ],
                [
                    'pilha_bateria' => 'Pilha Alcalina AAA Philips',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente' => 1200,
                    'tensao_sem_carga' => 1.338,
                    'tensao_com_carga' => 1.38,
                    'resistencia_carga' => 23.8,
                ],
                [
                    'pilha_bateria' => 'Pilha Alcalina Duracel AAA',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente' => 1200,
                    'tensao_sem_carga' => 0.809,
                    'tensao_com_carga' => 0.993,
                    'resistencia_carga' => 23.8,
                ],
                [
                    'pilha_bateria' => 'Bateria Unipower (Recarregável)',
                    'tensao_nominal' => 12.0,
                    'capacidade_corrente' => 7000,
                    'tensao_sem_carga' => 10.35,
                    'tensao_com_carga' => 10.50,
                    'resistencia_carga' => 23.8,
                ],
                [
                    'pilha_bateria' => 'Bateria Freedown (Recarregável)',
                    'tensao_nominal' => 12.0,
                    'capacidade_corrente' => 30000,
                    'tensao_sem_carga' => 10.49,
                    'tensao_com_carga' => 10.69,
                    'resistencia_carga' => 23.8,
                ],
            ]


        );
        
    }
}
